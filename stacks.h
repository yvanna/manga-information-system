#include <iostream>
#include <conio.h>
#include <list>
#include <stdlib.h>

using namespace std;
class Node {
public:
		string	data;
		Node*	next;
};

class List{
	public:
			List(void)	{head = NULL;	}
			~List(void);
			
			bool isEmpty(){ return head == NULL;	}
			Node* createnode(int index,string x);
			int FindNode(string x);
			int DeleteNode(int index);
			int DeleteNodeforStacks(int index);
			void DisplayList(void);
			string ViewMusic(int index);
			
private:
	Node* head;
	friend class Stack;
};

// for stack
class Stack : public List {
public:
	Stack() {}
	~Stack() {}
	void Top() 
	{
		if (head == NULL) 
		{
			cout << "Error: the stack is empty." <<endl;
		}
		else
		{
			cout<< head->data <<endl;
		}
	}
	void Push(string x) {createnode(0,x); }
	void Pop() 
	{
		if (head == NULL) 
		{
			cout << "Error: the stack is empty." <<endl;
		}
		else 
		{	
			int value = 0;
			DeleteNodeforStacks(value);
		}
	}

	void DisplayStack() { (DisplayList()); }
};

// for linkedlist
int List::FindNode(string x){
	Node* CurrNode	=	head;
	int	CurrIndex	=	1;
	while (CurrNode && CurrNode->data != x){
		CurrNode	=	CurrNode->next;
		CurrIndex++;
	}
	if (CurrNode) return CurrIndex;
	return 0;
}

//this is for deleting
int List:: DeleteNode(int index)
{
	Node* prevNode	=	NULL;
	Node* CurrNode	=	head;
	int CurrIndex	=	1;
	while (CurrIndex != index)
	{
		prevNode	=	CurrNode;
		CurrNode	=	CurrNode->next;
		CurrIndex++;
	}
	if (CurrNode)
	{
		if(prevNode)
		{
			prevNode->next	=	CurrNode->next;
			delete CurrNode;
		}
		else
		{
			head	=	CurrNode->next;
			delete	CurrNode;
		}
		return	CurrIndex;
	}
	return 0;
}

string List::ViewMusic(int index){
	Node* CurrNode	=	head;
	int	CurrIndex	=	1;
	while (CurrIndex != index){
		CurrNode	=	CurrNode->next;
		CurrIndex++;
	}
	if (CurrNode) return CurrNode->data;
	return NULL;
}

void List::DisplayList(){
	int num = 0;
	Node* CurrNode	=	head;
	while (CurrNode != NULL){
		cout<<"["<<num+1<<"]"<<CurrNode->data<<endl;
		CurrNode	=	CurrNode->next;
		num++;
	}
	
}
Node* List::createnode(int index,string x){
	if (index < 0) return NULL;
	int CurrIndex = 1;
	Node* CurrNode =head;
	while (CurrNode && index > CurrIndex){
		CurrNode	=	CurrNode->next;
		CurrIndex++;
	}
	Node* newNode =		new		Node;
	newNode->data = 	x;
	if (index==0){
			newNode->next	=	head;
			head 			=	newNode;
	}
	else{
		newNode->next	=	CurrNode->next;
		CurrNode->next	=	newNode;
	}
	return newNode;
}

List::~List(void){
	Node* CurrNode	=	head,	*nextNode	=	NULL;
	while (CurrNode != NULL){
		nextNode	=	CurrNode->next;
		delete	CurrNode;
		CurrNode	=	nextNode;
	}
}

int List:: DeleteNodeforStacks(int index)
{
	Node* prevNode	=	NULL;
	Node* CurrNode	=	head;
	int CurrIndex	=	0;
	while (CurrIndex != index)
	{
		prevNode	=	CurrNode;
		CurrNode	=	CurrNode->next;
		CurrIndex++;
	}
	if (CurrNode)
	{
		if(prevNode)
		{
			prevNode->next	=	CurrNode->next;
			delete CurrNode;
		}
		else
		{
			head	=	CurrNode->next;
			delete	CurrNode;
		}
		return	CurrIndex;
	}
	return 0;
}


