#include <iostream>
#include <conio.h>
#include <list>
#include <stdlib.h> 
using namespace std;

class Node {
public:
		string	data;
		Node*	next;
};
class List{
	public:
			List(void)	{head = NULL;	}
			~List(void);
			
			bool isEmpty(){ return head == NULL;	}
			Node* createnode(int index,string x);
			int FindNode(string x);
			int DeleteNode(int index);
			void DisplayList(void);
			void DisplayPlaylist(void);
			string ViewMusic(int index);
	private:
			Node* head;
};
int List::FindNode(string x){
	Node* CurrNode	=	head;
	int	CurrIndex	=	1;
	while (CurrNode && CurrNode->data != x){
		CurrNode	=	CurrNode->next;
		CurrIndex++;
	}
	if (CurrNode) return CurrIndex;
	return 0;
}
int List:: DeleteNode(int index){
	Node* prevNode	=	NULL;
	Node* CurrNode	=	head;
	int CurrIndex	=	1;
	while (CurrIndex != index){
		prevNode	=	CurrNode;
		CurrNode	=	CurrNode->next;
		CurrIndex++;
	}
	if (CurrNode){
		if(prevNode){
			prevNode->next	=	CurrNode->next;
			delete CurrNode;
		}
		else{
			head	=	CurrNode->next;
			delete	CurrNode;
		}
		return	CurrIndex;
	}
	return 0;
}
string List::ViewMusic(int index){
	Node* CurrNode	=	head;
	int	CurrIndex	=	1;
	while (CurrIndex != index){
		CurrNode	=	CurrNode->next;
		CurrIndex++;
	}
	if (CurrNode) return CurrNode->data;
	return NULL;
}
void List::DisplayPlaylist(){
	int num = 0;
	Node* CurrNode	=	head;
	while (CurrNode != NULL){
		cout<<CurrNode->data<<endl;
		CurrNode	=	CurrNode->next;
		num++;
	}
	
}
void List::DisplayList(){
	int num = 0;
	Node* CurrNode	=	head;
	while (CurrNode != NULL){
		cout<<"["<<num+1<<"]"<<CurrNode->data<<endl;
		CurrNode	=	CurrNode->next;
		num++;
	}
	
}
Node* List::createnode(int index,string x){
	if (index < 0) return NULL;
	int CurrIndex = 1;
	Node* CurrNode =head;
	while (CurrNode && index > CurrIndex){
		CurrNode	=	CurrNode->next;
		CurrIndex++;
	}
	Node* newNode =		new		Node;
	newNode->data = 	x;
	if (index==0){
			newNode->next	=	head;
			head 			=	newNode;
	}
	else{
		newNode->next	=	CurrNode->next;
		CurrNode->next	=	newNode;
	}
	return newNode;
}
List::~List(void){
	Node* CurrNode	=	head,	*nextNode	=	NULL;
	while (CurrNode != NULL){
		nextNode	=	CurrNode->next;
		delete	CurrNode;
		CurrNode	=	nextNode;
	}
}
int main(){
	string mus,num;
	List obj;
	obj.createnode(0, "Lean on in by Johny Houlihan Ft. Briana Tryson");
	obj.createnode(1, "Couldn't love you more by Johny Houlian");
	obj.createnode(2, "I'm falling in love by Wildflowers Ft Emmi");
	obj.createnode(3, "We're a little messed up by Wildflowers ft. Emmi");
	obj.createnode(4, "I wont waste this by Wildflowers Ft. Russel");
	obj.createnode(5, "Paradise by Elias Naslin");
	obj.createnode(6, "Tenki no ko by Radwimps");
	obj.createnode(7, "Sparkle by Radwimps");
	obj.createnode(8, "We are by One Ok Rock");
	obj.createnode(9, "I was a King by One Ok Rock");
	again:
	
	system("CLS");
    cout << "===============Welcome to your Music Database================" << endl;
    cout << "	Please choose the category you'd like to do." << endl;
    cout << "	1.  See the list of the music you have stored in the system." << endl;
    cout << "	2.  Add a new music in your playlist." << endl;
    cout << "	3.  Delete a music in your playlist." << endl;
    cout << "	4.  Edit one of the music you have in your playlist." << endl;
    cout << "	5.  View what is currently playing in your playlist." << endl;
    cout << "	6.  Exit." << endl;
    cout << "===================================================================" << endl;
    cout<< "Your choice is: ";
	cin>>num;

	if(num=="1")
	{
		system("CLS");
		cout<<"\n--------------------------------------------------\n";
		cout<<"---------------Displaying All songs-----------------";
		cout<<"\n--------------------------------------------------\n";
		obj.DisplayPlaylist();
		cout<<endl;
		cout<<"\n--------------------------------------------------\n";
		cout<<"\n--------------------------------------------------\n";
		system("PAUSE");
		goto again;
	}
	
	else if (num=="2")
	{
		system("CLS");	
		string mus;
		cout<<"Type the title of the music you want to add: "<<endl;
		cin.ignore();
		getline(cin,mus);
		cout<<"\n--------------------------------------------------\n";
		cout<<"-----------------Displaying the new playlist-----------------";
		cout<<"\n--------------------------------------------------\n";
		obj.createnode(0,mus);
		obj.DisplayPlaylist();
		cout<<"\n--------------------------------------------------\n";
		cout<<"\n--------------------------------------------------\n";
		system("PAUSE");
		goto again;
	}

	else if(num=="3")
	{
		system("CLS");
		int del;
		cout<<"\n--------------------------------------------------\n";
		cout<<"---------------Displaying All songs-----------------";
		cout<<"\n--------------------------------------------------\n";
		obj.DisplayList();
		cout<<"\n--------------------------------------------------\n";
		cout<<"\n--------------------------------------------------\n";
		cout<<"Choose the number of the music title you want to delete : "<<endl;
		cin>>del;
		cout<<"\n--------------------------------------------------\n";
		cout<<"---------------Displaying All songs-----------------";
		cout<<"\n--------------------------------------------------\n";
		obj.DeleteNode(del);
		obj.DisplayList();
		cout<<"\n--------------------------------------------------\n";
		cout<<"\n--------------------------------------------------\n";
		system("PAUSE");
		goto again;
	}
	
	else if(num=="4")
	{
		system("CLS");
		cout<<"\n--------------------------------------------------\n";
		cout<<"---------------Displaying All songs-----------------";
		cout<<"\n--------------------------------------------------\n";
		obj.DisplayList();
		cout<<"\n--------------------------------------------------\n";
		cout<<"\n--------------------------------------------------\n";
		int editnum;
		string editname;
		cout<<"Choose the number of the music title you want to edit : "<<endl;
		cin>>editnum;
	
		obj.DeleteNode(editnum);
		cout<<"Enter the new music title : ";
		cin.ignore();
		getline(cin,editname);
		cout<<endl;
		cout<<"\n--------------------------------------------------\n";
		cout<<"---------------Displaying All songs-----------------";
		cout<<"\n--------------------------------------------------\n";
		obj.createnode(editnum-1,editname);
		obj.DisplayList();
		cout<<"\n--------------------------------------------------\n";
		cout<<"\n--------------------------------------------------\n";
		system("PAUSE");
		goto again;
	}
	
	else if(num=="5")
	{
		system("CLS");
		int musnum;
		cout<<"\n--------------------------------------------------\n";
		cout<<"---------------Displaying All songs-----------------";
		cout<<"\n--------------------------------------------------\n";
		obj.DisplayList();
		cout<<"\n--------------------------------------------------\n";
		cout<<"\n--------------------------------------------------\n";
		cout<<"Choose the number of the music you want to play : "<<endl;
		cin>>musnum;
		cout<<"Now Playing : "<<obj.ViewMusic(musnum)<<endl;
		cout<<"Next : "<<obj.ViewMusic(musnum+1)<<endl;
		cout<<"Previous : "<<obj.ViewMusic(musnum-1)<<endl;	
		system("PAUSE");
		goto again;
	}

	else if(num=="6"){
		return 0;
	}
	else{
		cout<<"Enter a number based on the instructions\nPlease Try Again\n\n<---------------------------------------------------------------------->\n\n"<<endl;
		goto again;
	}
}
