#include <iostream>
#include <bits/stdc++.h> 
using namespace std; 
  
int main() 
{ 
	string movie_title[26] = 
	{
	"0" , "That Thing Called Tadhana" , "Starting Over Again" , "One More Chance" , "Kita Kita" , "Four Sisters and a Wedding" , 
	"Seven Sundays" , "Hello, Love, Goodbye" , "Goyo: Ang Batang Heneral" , "My Ex and Whys" , "Pagpag: Siyam na Buhay" , 
	"The Breakup Playlist" , "Vince and Kath and James" , "The Hows of Us" , "A Second Chance" , "100 Tula para kay Stella" , 
	"Alone/Together" , "Indak" , "Ulan" , "Never Not Love You" , "Kimi no Suizou wo Tabetai (I Want to Eat Your Pancreas)" , 
	"Koe no Katachi (A Silent Voice)" , "Howl's Moving Castle" , "Detective Conan: Zero the Enforcer" , 
	"Detective Conan: The Fist of Blue Sapphire" , "Kimi no Na Wa (Your Name)"
	}; 
	
	string director[26] = 
	{
	"0" , "Antoinette Jadaone" , "Olivia M. Lamasan" , "Cathy Garcia-Molina" , "Sigrid Andrea Bernardo" , "Cathy Garcia-Molina" , 
	"Cathy Garcia-Molina" , "Cathy Garcia-Molina" , "Jerrold Tarog" , "Cathy Garcia-Molina" , "Frasco S. Mortiz" , "Dan Villegas" , 
	"Theodore Boborol" , "Cathy Garcia-Molina" , "Cathy Garcia-Molina" , "Jason Paul Laxamana" , "Antoinette Jadaone" , 
	"Paul Alexei Basinillo" , "Irene Villamor" , "Antoinette Jadaone" , "Shin'ichiro Ushijima" , "Naoko Yamada" , 
	"Hayao Miyazaki" , "Tachikawa Yuzuru" , "Tomoka Nagaoka" , "Makoto Shinkai"
	}; 
	
	string producer[26] = 
	{
	"0" , "Bianca Balbuena and Dan Villegas" , "Star Cinema" , "Elma Madua" , "Erwin Blanco" , "Star Cinema" , "Star Cinema" , 
	"Carlo Katigbak and Olivia Lamasan" , "Joenathann Alandy and Daphne O. Chiu" , "Charo Santos-Concio" , 
	"Charo Santos-Concio and Roselle Y. Monteverde" , "Charo Santos-Concio" , "Star Cinema" , 
	"Olivia Lamasan	and Carlo Katigbak" , "Malou Santos" , "Viva Films" , "Carlo Katigbak	and Olivia Lamasan" , "Viva Films" , 
	"Viva Films" , "Viva Films" , "Keji Mita" , "Kyoto Animation" , "Toshio Suzuki" , "TMS Entertainment" , "TMS Entertainment" , 
	"CoMix Wave Films"
	}; 
      
    string main_actor[26] =
	{  
    "0" , "Angelica Panganiban and JM de Guzman" , "Piolo Pascual and Tony Gonzaga" , "John Lloyd Cruz and Bea Alonzo" , 
	"Alessandra de Rossi and Empoy Marquez" , "Toni Gonzaga, Bea Alonzo, Angel Locsin, Shaina Magdayao and Enchong Dee" , 
	"Aga Muhlach, Ronaldo Valdez, Cristine Reyes, Dingdong Dantes and Enrique Gil" , "Kathryn Bernardo and Alden Richards" , 
	"Paulo Avelino" , "Enrique Gil and Liza Soberano" , "Kathryn Bernardo, Daniel Padilla, Paulo Avelino and Shaina Magdayao" , 
	"Piolo Pascual and Sarah Geronimo" , "Julia Barretto, Joshua Garcia and Ronnie Alonte" , "Kathryn Bernardo and Daniel Padilla" , 
	"John Lloyd Cruz and Bea Alonzo" , "Bela Padilla and JC Santos" , "Liza Soberano and Enrique Gil" , "Sam Concepcion	and Nadine Lustre" , 
	"Carlo Aquino and Nadine Lustre" , "James Reid and Nadine Lustre" , "Lynn as Sakura Yamauchi & Mahiro Takasugi as Haruki Shiga" , 
	"Miyu Irino as Shoya Ishida & Saori Hayami as Shoko Nishimiya" , "Chieko Baisho as Sophie & Takuya Kimura as Howl (Hauru)" , 
	"Minami Takayama as Conan Edogawa & Rikiya Koyama as Kogoro Mori & Toru Furuya as Rei Furuya / Toru Amuro" , 
	"Minami Takayama as Conan Edogawa / Arthur Hirai & Rikiya Koyama as Kogoro Mori & Kappei Yamaguchi as and Kaito Kid" , 
	"Ryunosuke Kamiki as Taki Tachibana and Mone Kamishiraishi as Mitsuha Miyamizu"
	};

	string genre[26] = 
	{
	"0" , "Romance and Comedy" , "Romance and Drama" , "Romance and Drama" , "Romance and Comedy" , "Romance and Comedy" , 
	"Drama" , "Drama and Romance" , "History and Epic" , "Drama and Romance" , "Horror" , "Drama, Music and Romance" , "Romance" , 
	"Drama and Romance" , "Romance and Drama" , "Romance" , "Drama and Romance" , "Drama and Romance" , "Drama and Fantasy" , 
	"Drama and Romance" , "Animation, Drama and Romance" , "Animation, Drama and Romance" , "Animation, Drama and Fantasy" , 
	"Animation, Drama and Thriller" , "Animation and Action" , "Animation, Romance and Drama"
	};
	
	string year [26] = 
	{
	"0" , "2014", "2014" , "2007" , "2017" , "2013" , "2017" , "2019" , "2018" , "2017" , "2013" , "2015" , "2016" , 
	"2018" , "2015" , "2017" , "2019" , "2019" , "2019" , "2018" , "2018" , "2016" , "2004" , "2018" , "2019" , "2016"
	};
	
	string description [26] = 
	{
	"0" , 
	"A story about a broken-hearted girl who meets a boy in a not so normal way. Together, they go to places and find out where do broken hearts go?" ,
	"A pair of exes run into each other again years after a breakup that left them with plenty of unresolved questions." , 
	"Longtime couple Basha (Bea Alonzo) and Popoy (John Lloyd Cruz) are practically inseparable, so when they split up, it's not surprising how heartbroken each feels. But Basha, stifled by the relationship, wants to spread her wings, and Popoy loves her too much to stand in her way. Struggling to build their lives anew, the onetime lovers face daily reminders of their happier times together even as they try to chart their own paths." , 
	"In one horrible moment, her entire world clouds over. It takes an unlikely stranger to help her see its beauty again." , 
	"Three sisters reunite to dissuade their younger brother from marrying his fiancee. As they interact, they face the feelings and issues they have kept buried for a long time." , 
	"Siblings reunite when their father is diagnosed with cancer, and must settle old issues as they spend time together." , 
	"Joy is the quintessential OFW, working hard in Hong Kong to provide for her family in the Philippines, with aspirations of migrating to Canada. Ethan is less driven, working as a bartender as he waits for his residency to push through. Joy has shied away from relationships, but is unable to resist Ethan. When they fall in love, they both agree that the relationship would be temporary. But as their bond deepens, would they be able to keep to their agreement?" , 
	"The life of Gregorio `Goyo' del Pilar, one of the youngest generals during the Philippine-American War, who fought in the Battle of Tirad Pass." , 
	"Popular blogger Cali is hired to work on a marketing campaign alongside another viral sensation, who also happens to be her ex-boyfriend. Former playboy Gio is intent on proving he has changed and winning Cali back." , 
	"A spirit of a recent dead man hunts down nine persons and tries to kill them after the victims ignored some superstitions during his wake. One of it being that no one should go home directly after visiting a wake so that the spirit of the dead will not follow." , 
	"A story about an aspiring professional singer and a rock singer who collaborates in a song. As they work on their song, they start to develop feelings for each other." , 
	"Love can be complicated, especially when Vince agrees to secretly woo Kath via text on behalf of James, while falling for her, too." , 
	"A young couple dream of growing old together as they deal with the struggles of being in a long-term relationship." , 
	"After their storybook wedding, Popoy and Basha find married life, and starting a business, more challenging than they ever imagined." , 
	"Throughout his four years in college, Fidel, a stuttering student, tries to finish 100 poems dedicated to Stella, an aspiring but frustrated rock star, to win her heart." , 
	"After meeting again at an awards ceremony, a former couple discover they still have a connection." , 
	"A girl is invited to join Indak Pinas, a dance group, after a dance video she posts goes viral." , 
	"Maya is a girl who has always held a pessimistic view of the rain, reminding her of failed love and other depressing things. Will the rain ever stop her in her journey to overcome past heartaches?" , 
	"The story of Gio and Joanne whose young, carefree and reckless love is tested when their dreams took them to different paths." , 
	"An aloof boy comes across a book in a hospital waiting room. He soon discovers that it is a diary kept by his very popular classmate who reveals to him that she is secretly suffering from a fatal pancreatic illness." , 
	"When a grade school student with impaired hearing is bullied mercilessly, she transfers to another school. Years later, one of her former tormentors sets out to make amends." , 
	"Sophie has an uneventful life at her late father's hat shop, but all that changes when she befriends wizard Howl, who lives in a magical flying castle. However, the evil Witch of Waste takes issue with their budding relationship and casts a spell on young Sophie, which ages her prematurely. Now Howl must use all his magical talents to battle the jealous hag and return Sophie to her former youth and beauty." , 
	"A young crime solver embarks on a secret mission to clear a detective's name when someone frames him for an explosion that kills several officers." , 
	"The world's greatest blue sapphire, the 'blue lapis fist', said to have sunk in a pirate ship in the late 19th century, on the coasts of Singapore. A local millionaire plots to retrieve it, and when it's exhibited in an exhibition at the Singaporean Marina Sands hotel, a murder takes place." , 
	"Two strangers find themselves linked in a bizarre way. When a connection forms, will distance be the only thing to keep them apart?"
	};

	string status[26] = 
	{
	"0" , "AVAILABLE  ", "AVAILABLE  ", "AVAILABLE  ", "AVAILABLE  ", "AVAILABLE  ", "AVAILABLE  ", "AVAILABLE  ", "AVAILABLE  ",
	"AVAILABLE  ", "AVAILABLE  ", "AVAILABLE  ", "AVAILABLE  ", "AVAILABLE  ", "AVAILABLE  ", "AVAILABLE  ", "AVAILABLE  ",
	"AVAILABLE  ", "AVAILABLE  ", "AVAILABLE  ", "AVAILABLE  ", "AVAILABLE  ", "AVAILABLE  ", "AVAILABLE  ", "AVAILABLE  ",
	"AVAILABLE  "
	};
	
	string customers[41];
	string movie_rented[41];
	string date_rented[41];
	string customer_name;	
	int fc; 	//the chosen category to do
    int sc;		//for detailed information
    int mc; 	//for renting the movie (the number of the movie)
    int nc=0; 	//number of customers
    char yes = 'y';
    while (yes == 'y')
    {
    cout << "===============Welcome to the Movie Rental Database================" << endl;
    cout << "	Please choose the category you'd like to do." << endl;
    cout << "	1.  See the list of the movie titles stored in the system." << endl;
    cout << "	2.  See more information about a specific movie." << endl;
    cout << "	3.  Rent or Check-out movie from the list." << endl;
    cout << "	4.  Return the rented or already check-out movies." << endl;
    cout << "	5.  See the list of the customers who rented the movies." << endl;
    cout << "	6.  Exit." << endl;
    cout << "===================================================================" << endl;
    cout << "Your choice is ";
    cin >> fc;

	if (fc == 1)
	{
		cout << "------------------------MOVIE LIST---------------------" << endl <<endl;
		cout << "STATUS:            #:    TITLE:" << endl;
		for(int i=1; i<=25; i++)
			{
				cout << status[i] << "        " << i << ": "<< movie_title[i] << endl;
			}
		cout << "-------------------END OF THE LIST---------------------" << endl << endl;
	}

	else if (fc == 2)
	{
		cout << "---------------------MOVIE LIST------------------------" << endl <<endl;
		cout << "STATUS:            #:    TITLE:" << endl;
		for(int i=1; i<=25; i++)
		{
			cout << status[i] << "        " << i << ": "<< movie_title[i] << endl;
		}
		cout << "-------------------END OF THE LIST---------------------" << endl << endl;
		cout << "View a detailed information of your chosen movie by selecting its number: ";
		cin >> sc;
			if(sc <= 0 or sc >=26)
			{
				cout << "INVALID INPUT" << endl;
			}
			else
			{
				cout << "-----------------DETAILED INFORMATION ABOUT THE SELECTED MOVIE----------------" << endl << endl;
				cout << "STATUS: " << status[sc] << endl;
				cout << "TITLE: " << movie_title[sc] << endl;
				cout << "YEAR: " << year[sc] << endl;
				cout << "GENRE: " << genre[sc] << endl; 
				cout << "DIRECTORs: " << director[sc] << endl; 
				cout << "PRODUCERs: " << producer[sc] << endl;
				cout << "MAIN ACTORs: " << main_actor[sc] << endl; 
				cout << "DESCRIPTION: " << description[sc] << endl; 
				cout << "------------------------------------END---------------------------------------" << endl;
			}
		}
		
	else if (fc == 3)
	{
	    cout << "---------------------MOVIE LIST------------------------" << endl <<endl;
	    cout << "STATUS:            #:    TITLE:" << endl;
	    for(int i=1; i<=25; i++)
	    {
		    cout << status[i] << "        " << i << ": "<< movie_title[i] << endl;
	    }
	    cout << "-------------------END OF THE LIST---------------------" << endl << endl;
	int b;
	cout<<"Enter the number of the movie you want to rent:  ";
	cin>>b;
	status[b] = "UNAVAILABLE";
	cout<<"To rent that movie please enter your first name: ";
	string customer_name;
	cin>>customer_name;
	customers[nc] = customer_name;
	movie_rented[nc] = movie_title[b];
	time_t curr_time;
	tm * curr_tm;
	char date_string[100];
	time(&curr_time);
	curr_tm = localtime(&curr_time);
	strftime(date_string, 50, "%B %d, %Y", curr_tm);
	date_rented[nc] = date_string;
	cout<<movie_title[b]<<" has been rented by " <<customers[nc]<<" successfully on "<<date_rented[nc]<<"." <<endl;
	}
	
	else if (fc == 4)
	{
		cout << "--------------------------------RETURNING of MOVIES-----------------------------------" << endl <<endl;
		cout << "STATUS:            #:    TITLE:" << endl;
		for(int i=1; i<=25; i++)
		{
			cout << status[i] << "        " << i << ": "<< movie_title[i] << endl;
		}
		cout << "-----------------------------------END OF THE LIST------------------------------------" << endl << endl;
		bool x = true;
		while(x == true)
		{
			cout << "To return a movie, choose its number from the list: ";
			cin >> mc;
			if(mc <= 0 or mc >=26)
			{
				cout << "INVALID INPUT" << endl;
				x = true;
			}
			else
			{
			x = false;
			}
		}
		if (status[mc] == "AVAILABLE  ")
		{
			cout << "The movie that you have chosen is currently available, please check the number again." << endl;
		}
		else
		{
			status[mc] = "AVAILABLE  ";
			cout << "The movie you borrowed is successfully returned" << endl;
		}
	}
	
	else if (fc == 5)
	{
		cout << "--------------------------------RECORDS OF THE CUSTOMERS-----------------------------------" << endl <<endl;
		for(int ii=0; ii<=nc; ii++)
		{
			if (customers[nc] != "")
			{
				cout << "----------------------------------------------------" << endl;
				cout << "CUSTOMER NAME: " << customers[nc] << endl;
				cout << "MOVIE RENTED: " << movie_rented[nc] << endl;
				cout << "DATE RENTED: " << date_rented[nc] << endl;	
				cout << "----------------------------------------------------" << endl;
			}	
		}
	}
	
	else if (fc == 6)
	{
		cout << "Thank you for checking!" << endl;
		exit(0);
	}
	else
	{
		cout << "INVALID INPUT" << endl;
		system("PAUSE");
	}
    cout<<"Would you like to do another option (y if yes) > ";
    cin>>yes;
        
    }
	return 0;
} 
