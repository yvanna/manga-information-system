import time
print ("\nFor knowing an information about a certain manga, type the following numerical codes located beside its title.\n")
Titles_codes = [("A Good Day to be a Dog -- 23001",), ("Ace of the Diamond -- 64002",), ("Arifureta - From Commonplace to World's Strongest -- 23564",), ("Blue Spring Ride -- 33321",), ("Blue Wings -- 23145",), ("Case Closed -- 96785",), ("Chasing The Sun -- 54376",), ("Demon Slayer: Kimetsu No Yaiba -- 38265",), ("Food Wars: Shokugeki No Souma -- 56000",),  ("Haikyu -- 00234",), ("Prince of Tennis -- 37823",), ("Solo Leveling -- 38925",), ("Unordinary-- 87921",), ("World Teacher -- 03576",), ("Zero's Tea Time -- 23894",)]
for item in Titles_codes:
    print(*item)
Codes = ("23001", "64002", "23564", "33321", "23145", "96785", "54376", "38265", "56000", "00234", "37823", "38925", "87921", "03576", "23894")
Titles = ("A Good Day to be a Dog", "Ace of the Diamond", "Arifureta - From Commonplace to World's Strongest", "Blue Spring Ride", "Blue Wings", "Case Closed", "Chasing The Sun", "Demon Slayer: Kimetsu No Yaiba", "Food Wars: Shokugeki No Souma", "Haikyu", "Prince of Tennis", "Solo Leveling", "Unordinary", "World Teacher", "Zero's Tea Time")
Author = ("Lee Hye", "Yuuji Terajima", "Ryo Shirakome", "Io Sakisaka", "Xu Lu Ako", "Gosho Aoyama", "Zuo Xiao Ling", "Koyoharu Gotouge", "Yuuto Tsukuda", "Haruichi Furudate", "Takeshi Konomi", "REDICE Studio", "Uru-chan", "Neko Kouichi", "Takahiro Arai")
Chapters = ("92", "389", "36", "56", "78", "1043", "83", "184", "331", "385", "381", "83", "158", "23", "25")
Published = ("2017", "2006", "2016", "2011", "2014", "1994", "2018", "2016", "2012", "2012", "1999", "2018", "2016", "2019", "2018")
Price = ("745", "1755", "500", "620", "650", "4500", "700", "910", "1500", "1700", "2000", "750", "900", "425", "475")
Availability = ("Available", "Available", "Available", "Available", "Not Available", "Available", "Available", "Not Available", "Available", "Available", "Not Available", "Available", "Not Available", "Available", "Available")
time.sleep(1)
while True:
	found = False
	product_name=input("\nEnter the product code: ")
	for x in range(0,len(Codes)):
		if Codes[x] == product_name:
			print("TITLE: ", (Titles[x + 0]))
			print("AUTHOR: ", (Author[x + 0]))
			print("CHAPTERS: ", (Chapters[x + 0]))
			print("YEAR OF PUBLISHED: ", (Published[x + 0]))
			print("AVAILABILITY OF THE MANGA: ", (Availability[x + 0]))
			print("PRICE: ", (Price[x + 0]))
			found = True
	if found==False:
		print("Product not found")
	time.sleep(2)
	decision = input("\nDo you want to repeat (y/n):  ") 
	if decision != 'y' and decision != 'Y':
		break