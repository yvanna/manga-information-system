#include <iostream>
#include <conio.h>
#include <list>
#include <stdlib.h> 
#include "stacks.h"
#include "phrases.cpp" //some of the cout phrase

using namespace std;

int main(){
	List obj;
	Stack playlist;
	int counter = 0;
	obj.createnode(0, "Lean on in by Johny Houlihan Ft. Briana Tryson");
	obj.createnode(1, "Couldn't love you more by Johny Houlian");
	obj.createnode(2, "I'm falling in love by Wildflowers Ft Emmi");
	obj.createnode(3, "We're a little messed up by Wildflowers ft. Emmi");
	obj.createnode(4, "I wont waste this by Wildflowers Ft. Russel");
	obj.createnode(5, "Paradise by Elias Naslin");
	obj.createnode(6, "Tenki no ko by Radwimps");
	obj.createnode(7, "Sparkle by Radwimps");
	obj.createnode(8, "We are by One Ok Rock");
	obj.createnode(9, "I was a King by One Ok Rock");
	repeat:
	
	system("CLS");
	string option;
	welcome();
    cout<< "Your choice is: ";
	cin>>option;

//see the music library
	if(option == "1")
	{
		system("CLS");
		display_all();
		obj.DisplayList();
		cout<<endl;
		endline();
		system("PAUSE");
		goto repeat;
	}

//add a new music in music library
	else if (option=="2")
	{
		system("CLS");	
		string name_music;
		cout<<"Type the title of the music you want to add followed by its artist: "<<endl;
		cin.ignore();
		getline(cin,name_music);
		display_all();
		obj.createnode(0,name_music);
		obj.DisplayList();
		endline();
		system("PAUSE");
		goto repeat;
	}

//delete music in music library
	else if(option=="3")
	{
		system("CLS");
		int delete_music;
		display_all();
		obj.DisplayList();
		endline();
		cout<<"Choose the number of the music title you want to delete : "<<endl;
		cin>>delete_music;
		display_all();
		obj.DeleteNode(delete_music);
		obj.DisplayList();
		endline();
		system("PAUSE");
		goto repeat;
	}

//edit music in music library	
	else if(option=="4")
	{
		system("CLS");
		display_all();
		obj.DisplayList();
		endline();
		int edit_number;
		string edit_name;
		cout<<"Choose the number of the music title you want to edit : "<<endl;
		cin>>edit_number;
		
		obj.DeleteNode(edit_number);
		cout<<"Enter the new music title : ";
		cin.ignore();
		getline(cin,edit_name);
		cout<<endl;
		display_all();
		obj.createnode(edit_number-1,edit_name);
		obj.DisplayList();
		endline();
		system("PAUSE");
		goto repeat;
	}

//view currently playing	
	else if(option=="5")
	{
		system("CLS");
		int music_play_number;
		display_all();
		obj.DisplayList();
		endline();
		cout<<"Choose the number of the music you want to play : "<<endl;
		cin>>music_play_number;
		cout<<"Now Playing : "<<obj.ViewMusic(music_play_number)<<endl;
		cout<<"Next : "<<obj.ViewMusic(music_play_number+1)<<endl;
		cout<<"Previous : "<<obj.ViewMusic(music_play_number-1)<<endl;	
		system("PAUSE");
		goto repeat;
	}
	
//add to playlist
	else if(option == "6")
	{
		system ("CLS");
		obj.DisplayList();
		int chosen_music;
		cout<<"Choose the number of the music title you want to add on your playlist.";		
		cin>>chosen_music;
		playlist.Push(obj.ViewMusic(chosen_music));
		system("PAUSE");
		goto repeat;
	}

//delete latest added in playlist and print the remaining	
	else if(option == "7")
	{
		system ("CLS");
		playlist.Pop();
		playlist.DisplayStack();	
		system("PAUSE");
		goto repeat;
	}

//print the latest added music in playlist	
	else if(option == "8")
	{
		system ("CLS");
		cout << "This is the latest song added to your playlist." <<endl;
		playlist.Top();
		system ("PAUSE");
		goto repeat;
	}

//print the	music in playlist
	else if (option == "9")
	{	
		system ("CLS");
		cout<< "This is your playlist." <<endl;
		playlist.DisplayStack();
		system ("PAUSE");
		goto repeat;	
	}

//exit	
	else if (option == "10")
	{
		return 0;
	}
	
	else{
		cout<<"The number you have input is not on the options.\nPlease choose between 1 to 6 only.\n\n===================================================================\n\n"<<endl;
		system("PAUSE");
		goto repeat;
	}
}
