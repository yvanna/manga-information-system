#include <iostream>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <windows.h>
#pragma comment(lib,"winmm.lib")
using namespace std;

////////////////////CREATING MY LIST///////////////////
class list
{
	private:
		typedef struct node
		{
			int data;
			int dataPlaylist;
			int dataQueue;
			int dataGenre;
			string musicTitle;
			string musicArtist;
			string musicGenre;
			string musicAlbum;
			bool inPlaylist;
			bool inQueue;
			
			node* next;
		}* nodePtr;
		
		nodePtr head;
		nodePtr curr;
		nodePtr temp;
		nodePtr top;

	public:
		list();
		void pushNode(string musicTitle,string musicArtist ,string musicGenre ,string musicAlbum,int dataGenre);
		void display();
		int addToPlaylist(int addData, int counter);
		int remFrmPlaylist(int counter);
		void viewPlaylist();
		int enQueue(int qData,int counter);
		int deQueue();
		void sortByGenre();
		void printQueue(int queueCounter);
		void playQueue(int queueCounter);
		void playStack(int stackCounter);
};

//INSTANTIATING POINTERS//
list::list()
{
	temp = NULL;
	head = NULL;
	curr = NULL;
	top = NULL;
}

//PUSH NODE / CREATE NODE//
void list::pushNode(string musicTitle,string musicArtist ,string musicGenre ,string musicAlbum, int dataGenre)
{
	nodePtr n = new node;
	n->next = NULL;
	if(head!=NULL)
	{
		curr = head;
		temp = head;
		while(curr->next != NULL)
		{
			curr = curr->next;
			temp = curr;
		}
		curr->next =n;
		curr = curr->next;
		curr->data = temp->data+1;
		//PUT COMPONENTS OF NODE HERE
		system("CLS");
		cout << "Nodes are being added."<<endl
			 <<"      **       *******    *******    **  **   **   *****        " <<endl
			 <<"     * **      **     **  **     **  **  **** **  **    **    "     <<endl
			 <<"    *   **     **     **  **     **  **  ** ****  **          " <<endl
			 <<"   ********    **     **  **     **  **  **  ***  **   ****    " <<endl
			 <<"  *       **   **     **  **     **  **  **  ***  **     **       "  <<endl
			 <<" *         **  *******    ********   **  **  ***   ******        " <<endl;
	}
	else
	{
		head = n;
		n->data =0;
	}
	top = n;
	n->musicTitle = musicTitle;
	n->musicArtist = musicArtist;
	n->musicGenre = musicGenre;
	n->musicAlbum = musicAlbum;
	n->inPlaylist = false;
	n->inQueue = false;
	n->dataPlaylist =0;
	n->dataQueue =0;
	n->dataGenre = dataGenre;
}

//DISPLAY STACK//
void list::display()
{
	cout << "COUNTER: "<<top->data <<endl; //top data = the index of the music
	cout << "=======================================LIBRARY=======================================" << endl;
	for(int i = 1;i<=top->data;i++)
	{
		curr=head;
		temp=head;
		while(curr!=NULL&&curr->data!=i)
		{
			curr = curr->next;
		}
		if(curr==NULL||curr->data==0)
		{
		}
		else
		{
			cout << curr->data << " " << curr->musicTitle << " in the album called " << curr->musicAlbum << " with the genre of " <<curr->musicGenre << endl;
		}
	} 
	cout << "======================================================================================" << endl;
}

//VIEWING PLAYLIST//
void list::viewPlaylist()
{
	cout << "=======================================PLAYLIST=======================================" << endl;
	for(int i = top->data;i>=0;i--)
	{
		curr=head;
		temp=head;
		while(curr!=NULL&&curr->dataPlaylist!=i)
		{
			curr = curr->next;
		}
		if(curr==NULL||curr->data==0||curr->dataPlaylist==0||curr->inPlaylist==false)
		{
		}
		else
		{
			cout << curr->dataPlaylist << " " << curr->musicTitle << " in the album called " << curr->musicAlbum << " with the genre of " <<curr->musicGenre << endl;
		}
	}
	cout << "======================================================================================" << endl;
}

//VIEWING QUEUE//
void list::printQueue(int queueCounter)
{
	cout << "========================================QUEUE=========================================" << endl;
	if(queueCounter==1)
	{
		cout << "EMPTY" <<endl;
	}
	else
	{
		for(int i=0;i<=top->data;i++)
		{
			curr=head;
			temp=head;
			while(curr!=NULL&&curr->dataQueue!=i)
			{
				curr = curr->next;
			}
			if(curr==NULL||curr->data==0||curr->dataQueue==0||curr->inQueue==false)
			{
			}
			else
			{
				cout << curr->dataQueue << " " << curr->musicTitle << " in the album called " << curr->musicAlbum << " with the genre of " <<curr->musicGenre << endl;
			}
		}	
	}
	cout << "======================================================================================" << endl;
}

//ADDING TO PLAYLIST//
int list::addToPlaylist(int addData, int counter)
{
	curr=head;
	temp=head;
	while(curr != NULL && curr-> data != addData)
	{
		temp = curr;
		curr = curr->next;
	}
	if (curr == NULL or curr->data ==0)
	{
		cout << addData << " data was not on the list \n";
		return 0;
	}
	else if(curr->inPlaylist==true)
	{
		return 0;
	}
	else
	{	
		curr->dataPlaylist = counter;
		curr->inPlaylist =true;
		return 1; 
	}
}

//REMOVING FROM PLAYLIST//
int list::remFrmPlaylist(int counter)
{
	int delData = counter -1;
	curr=head;
	temp=head;
	while(curr != NULL && curr-> dataPlaylist != delData)
	{
		temp = curr;
		curr = curr->next;
	}
	if (curr == NULL or curr->data ==0)
	{
		return 0;
	}
	else if(curr->inPlaylist==false)
	{
		return 0;
	}
	else
	{	
		cout << "REMOVED"<< endl;
		curr->dataPlaylist = 0;
		curr->inPlaylist =false;
		return 1; 
	}
}

//ENQUEUE / ADD TO QUEUE
int list::enQueue(int qData,int counter)
{
	curr=head;
	temp=head;
	while(curr != NULL && curr-> data != qData)
	{
		temp = curr;
		curr = curr->next;
	}
	if (curr == NULL or curr->data ==0)
	{
		cout << qData << " data was not on the list \n";
		return 0;
	}
	else if(curr->inQueue==true)
	{
		return 0;
	}
	else
	{	
		curr->dataQueue = counter;
		curr->inQueue =true;
		return 1; 
	}
}

//DEQUEUE / REMOVE FROM QUEUE
int list::deQueue()
{
	curr=head;
	while(curr->next!=NULL)
	{
		if(curr->dataQueue!=0)
		{
			curr->dataQueue=curr->dataQueue-1;
			if(curr->dataQueue==0)
			{
				curr->inQueue=false;			
			}
		}
		curr=curr->next;
	}
	return 1;
}

//PLAY PLAYLIST
void list::playStack(int stackCounter)
{
	int i=1;
	while(i<=stackCounter)
	{
		if(stackCounter==1)
		{
			break;
		}
		system("CLS");
		cout << "=======================================PLAYLIST=======================================" << endl;
		for(int x = top->data;x>=0;x--)
		{
			curr=head;
			temp=head;
			while(curr!=NULL&&curr->dataPlaylist!=x)
			{
				curr = curr->next;
			}
			if(curr==NULL||curr->data==0||curr->dataPlaylist==0||curr->inPlaylist==false)
			{
			}
			else
			{
				cout << curr->dataPlaylist << " " << curr->musicTitle << " in the album called " << curr->musicAlbum << " with the genre of " <<curr->musicGenre << endl;
			}
		}
		curr=head;
		while(i-1!=curr->dataPlaylist)
		{
			curr=curr->next;
		}
		curr=curr->next;
		int choice;
		cout << "=======================================PLAYLIST========================================="<<endl
			 << "|      CHOOSE:                                                                         |"<< endl
			 << "|      1:NEXT                                                                          |"<< endl
			 << "|      2:BACK                                                                          |"<< endl																  
			 << "========================================================================================"<< endl
			 <<"NOW PLAYING: "<< curr->musicTitle <<endl;//PLAYING SONG HERE
		//PLAY MUSIC FUNCTION FUCKING SHIT	
		cout << "INPUT: ";
		cin >> choice;
		if(choice==1)
		{
			i=i+1;
		}
		else
		{
			break;
		}	
	}
}

//PLAY QUEUE
void list::playQueue(int queueCounter)
{
	int i=1;
	while(i<=queueCounter)
	{
		if(queueCounter==1)
		{
			break;
		}
		system("CLS");
		cout << "========================================QUEUE=========================================" << endl;
		if(queueCounter==1)
		{
			cout << "EMPTY" <<endl;
		}
		else
		{
			for(int x=0;x<=top->data;x++)
			{
				curr=head;
				temp=head;
				while(curr!=NULL&&curr->dataQueue!=x)
				{
					curr = curr->next;
				}
				if(curr==NULL||curr->data==0||curr->dataQueue==0||curr->inQueue==false)
				{
				}
				else
				{
					cout << curr->dataQueue << " " << curr->musicTitle << " in the album called " << curr->musicAlbum << " with the genre of " <<curr->musicGenre << endl;
				}
			}	
		}
		cout << "======================================================================================" << endl;
		
		curr=head;
		while(i-1!=curr->dataQueue)
		{
			curr=curr->next;
		}
		curr=curr->next;
		int choice;
		cout << "========================================QUEUE==========================================="<<endl
			 << "|      CHOOSE:                                                                         |"<< endl
			 << "|      1:NEXT                                                                          |"<< endl
			 << "|      2:BACK                                                                          |"<< endl																  
			 << "========================================================================================"<< endl
			 <<"NOW PLAYING: "<< curr->musicTitle <<endl;//PLAYING SONG HERE
		//PLAY MUSIC FUNCTION
		cout << "INPUT: ";
		cin >> choice;
		if(choice==1)
		{
			i=i+1;
		}
		else
		{
			break;
		}	
	}	
}
// SELECTION SORTING
void list::sortByGenre()
{
	int i = 1;
	for(i;i<=13;i++)
	{
		curr=head;
		temp=head;
		while(curr->next!=NULL)//it will scan the whole 
		{
			if(curr->dataGenre == i)//i is the genre, 5=Pop, 4-Alternative, etc.. there are 13 genre
		
			{
				cout << curr->musicGenre <<" is the genre of " << curr->musicTitle << " in the album called " << curr->musicAlbum <<endl;
			}
			curr=curr->next;
		}
	}
}


