#include <iostream>
using namespace std;

//opening statement
void welcome(){
	cout << "=================Welcome to your Music Database====================" << endl;
    cout << "	Please choose the category you'd like to do." << endl;
    cout << "	1.  See the list of the music you have stored in the system." << endl;
    cout << "	2.  Add a new music in your music library." << endl;
    cout << "	3.  Delete a music in your music library." << endl;
    cout << "	4.  Edit one of the music you have in your music library." << endl;
    cout << "	5.  View what is currently playing in your music library." << endl;
	cout << "	6.  Add to playlist." << endl;
	cout << "	7.  Delete your latest music added to playlist" <<endl;
	cout << "	8.  View the latest music you add in your playlist" <<endl;
    cout << "	9.  View the playlist." << endl;
	cout << "	10.  Exit." << endl;
    cout << "===================================================================" << endl;
}

void endline()
{
	cout<<"\n--------------------------------------------------\n";
	cout<<"--------------------------------------------------\n";
}

void display_all()
{
	cout<<"\n--------------------------------------------------\n";
	cout<<"---------------Displaying All songs---------------";
	cout<<"\n--------------------------------------------------\n";
}
