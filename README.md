#Data Structures

**1.Manga Information System**
    A system where a user will type the codes of the manga he/she wants to know. Typing the proper code of the manga he/she has chosen, five information will be displayed added the knowledge of its availability whether it is open to buying the said manga.
    **FILE NAME: mangainformationsystem.py**

**2.Movie Rental Dvd System - using string** (one update left)
    A system where a user can rent or check out a stored movie. Added to this, he/she can also see detailed information about the following movies listed in the system.
    **FILE NAME: movie_dvd_rental.cpp**

**3.Movie Rental Dvd System - using linked list** (updating...)
    A system where a user can rent or check out a stored movie. Added to this, he/she can also see detailed information about the following movies listed in the system.
    **FILE NAME: movie_rental_dvd_pt2.cpp**
    
**4.Music System - using linked list** (not updated)
    **FILE NAME: music_system_linkedlist.cpp**

**5.Music System (Music Library and Playlist)**
    Music Library is made by using linked list while the Playlist is made by using stacks. Music Library consists of the following, viewing the whole music list, adding, editing and deleting music and viewing the current, previous and next music. While Playlist consist of the options Push (adding), Pop (deleting), Top (viewing latest in or added) and Viewing all songs in playlist.
    **FILE NAME : musiclibrary.cpp , stacks.h , phrases.cpp**
    
**6.Music Player (Folder)**
    This folder is made for our finals. The following codes in this folder is for making use the ADT like stacks, linked list, queue, sort and binary tree.